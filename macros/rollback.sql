{% macro rollback() %}

    {% set sql='alter schema dbt_smcintyre__gitlab_snowflake_stage swap with dbt_smcintyre__gitlab_snowflake_prod' %}
    {% do run_query(sql) %}
    {{ log("database swapped", info=True) }}

{% endmacro %}
